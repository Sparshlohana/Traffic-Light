function red() {
    for (let i = 60; i >= 0; i--) {
        setTimeout(() => {
            document.getElementById('red1').innerHTML = i;
        }, 1000 * (60 - i));
    }
}

function green() {
    for (let i = 30; i >= 0; i--) {
        setTimeout(() => {
            document.getElementById('green1').innerHTML = i;
        }, 1000 * (30 - i));
    }
}

function yellow() {
    for (let i = 3; i >= 0; i--) {
        setTimeout(() => {
            document.getElementById('yellow1').innerHTML = i;
        }, 1000 * (3 - i));
    }
}

red();
setTimeout(() => {
    green();
}, 60000);
setTimeout(() => {
    yellow()
}, 90000);


function refresh() {
    setTimeout("location.reload(true)", 93000)
}